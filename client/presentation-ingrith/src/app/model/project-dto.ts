export class ProjectDto {
  id?: number;
  technology?: string;
  title?: string;
  link?: string;
  description?: string;
  participation?: string;
  img?: string;
}
