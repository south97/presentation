import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class PresentationService extends HttpService<any>{

  constructor(  public httpClient: HttpClient,
    public router: Router) {    super(httpClient, environment.BaseUrl + '/project', router);
  }

  public all(): Observable<any>{
    return super.get('all');
  }
}
