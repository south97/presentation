import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class HttpService<T> {
  constructor(
    public httpClient: HttpClient,
    public url: String,
    public router: Router
  ) {}

  public changeUrl(newUrl: String) {
    this.url = newUrl;
  }

  protected get(endpoint: string, params?: {}): Observable<T[]> {
    return this.httpClient.get<T[]>(`${this.url}/${endpoint}`, params).pipe(
      map((response: any) => {
        console.log(response);

        return response;
      }),
      catchError((error) => {
        return this.errorHttp(error);
      })
    );
  }

  protected post(endpoint: string, body: T): Observable<any> {
    return this.httpClient.post<T[]>(`${this.url}/${endpoint}`, body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError((error) => {
        return this.errorHttp(error);
      })
    );
  }

  protected delete(endpoint: string, params?: {}): Observable<T[]> {
    return this.httpClient.delete<T[]>(`${this.url}/${endpoint}`, params).pipe(
      map((response: any) => {
        return response.payload;
      }),
      catchError((error) => {
        return this.errorHttp(error);
      })
    );
  }

  protected put(endpoint: string, body: T): Observable<any> {
    return this.httpClient.put<T[]>(`${this.url}/${endpoint}`, body).pipe(
      map((response: any) => {
        return response.payload;
      }),
      catchError((error) => {
        return this.errorHttp(error);
      })
    );
  }

  private errorHttp(error: any): Observable<any> {
    if (error instanceof HttpErrorResponse) {
      alert('Falló la conexión con el servidor');
      return throwError('Falló la conexión con el servidor');
    } else {
      return throwError(error.message);
    }
  }
}
