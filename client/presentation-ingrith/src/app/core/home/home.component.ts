import { Component, OnInit } from '@angular/core';
import { ProjectDto } from 'src/app/model/project-dto';
import { PresentationService } from 'src/app/services/presentation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  projects: ProjectDto[];
  constructor(private _presentationService: PresentationService) {}

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    this._presentationService.all().subscribe(
      (res) => {
        this.projects = res;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  openlink(link) {
    console.warn(link)
    window.location.href = link;
  }
}
