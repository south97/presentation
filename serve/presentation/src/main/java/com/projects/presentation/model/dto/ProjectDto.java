package com.projects.presentation.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectDto {
    private Integer id;
    private String technology;
    private String title;
    private String link;
    private String description;
    private String participation;
    private String img;
}
