package com.projects.presentation.service.impl;

import com.projects.presentation.model.dto.ProjectDto;
import com.projects.presentation.model.transformation.ProjectTransformation;
import com.projects.presentation.repository.ProjectRepository;
import com.projects.presentation.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectTransformation transformation;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectTransformation transformation) {
        this.projectRepository = projectRepository;
        this.transformation = transformation;
    }


    @Override
    public List<ProjectDto> getAllProject() {
        return projectRepository
                .findAll()
                .stream()
                .map(transformation::EntityToDto)
                .collect(Collectors.toList());
    }
}
