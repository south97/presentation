package com.projects.presentation.model.transformation;

import com.projects.presentation.model.dto.ProjectDto;
import com.projects.presentation.model.entity.Project;
import org.springframework.stereotype.Component;

@Component
public class ProjectTransformation {

    public ProjectDto EntityToDto(Project project) {
        return ProjectDto.builder()
                .id(project.getId())
                .description(project.getDescription())
                .link(project.getLink())
                .participation(project.getParticipation())
                .technology(project.getTechnology())
                .title(project.getTitle())
                .img(project.getImg())
                .build();
    }


}
