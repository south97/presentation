package com.projects.presentation.service;

import com.projects.presentation.model.dto.ProjectDto;

import java.util.List;

public interface ProjectService {
    List<ProjectDto> getAllProject();

}
